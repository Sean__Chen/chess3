#pragma once
#include "Piece.h"
class Rook :virtual public Piece
{
public:
	Rook(PieceName type,char color);
	~Rook();
	virtual bool isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const;
	virtual PieceName getType() const { return this->_type; }
	virtual char getColor() const { return this->_color; }

private:
	char _color;
	PieceName _type;
};

