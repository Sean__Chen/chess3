#pragma once
class Cube
{
public:
	Cube(int x, int y);
	Cube(const Cube& other) { *this = other; }
	~Cube();
	int getX() const;
	int getY() const;
	bool operator==(Cube& other) const;
	bool operator>(Cube& other) const { return ((this->getX() > other.getX()) && (this->getY() > other.getY())); }
	Cube& operator=(const Cube& other);
	void setX(int x) { this->_x = x; }
	void setY(int y) { this->_y = y; }


private:
	int _x;
	int _y;
};

