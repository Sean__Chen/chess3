#include "Knight.h"
#define KNIGHT_FIRST_MOVE 2
#define KNIGHT_SECOND_MOVE 1


bool Knight::isValidMove(Cube& src, Cube& dest, Piece* b[WIDTH][LENGTH]) const
{
	if (abs(dest.getY() - src.getY()) == KNIGHT_FIRST_MOVE)
	{
		if (abs(dest.getX() - src.getX()) != KNIGHT_SECOND_MOVE)
		{
			return false;
		}
	}
	else if (abs(dest.getX() - src.getX()) == KNIGHT_FIRST_MOVE)
	{
		if (abs(dest.getY() - src.getY()) != KNIGHT_SECOND_MOVE)
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	return true;
}
