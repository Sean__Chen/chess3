#include "Cube.h"



Cube::Cube(int x, int y)
{
	this->_x = x;
	this->_y = y;
}


Cube::~Cube()
{
}

int Cube::getX() const
{
	return this->_x;
}

int Cube::getY() const
{
	return this->_y;
}


bool Cube::operator==(Cube& other)const {
	return (this->_x == other.getX() && this->_y == other.getY());
}

Cube& Cube::operator=(const Cube& other) {
	this->setX(other.getX());
	this->setY(other.getY());
	return *this;
}
